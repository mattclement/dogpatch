# Dogpatch

View GitLab MR pseudo-patches from the comfort of neo{mutt,vim}


# Install
1. Build (`cargo build --release`) and put dogpatch somewhere in your PATH.
1. Get a Personal Access Token from your gitlab user with the API scope, export to your environment as `GL_TOKEN`.
1. Receive a merge request email. If it has the `X-GitLab-Project-Id` and `X-GitLab-MergeRequest-IID` headers, it will work!

# Usage
1. From the neomutt index view, `pipe` the message to `dogpatch` (default keybind is `|`).

You can create a macro in neomutt to open with dogpatch if you prefer. The following example works by pressing `p`. This overrides the default "print email" function.
```
macro p <pipe-message>dogpatch<enter>
```
