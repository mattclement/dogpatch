use mailparse::{parse_headers, MailHeaderMap};
use nix::unistd;
use serde::Deserialize;
use std::ffi::CString;
use std::io::{self, Read};

#[derive(Deserialize, Debug)]
struct Change {
    old_path: String,
    new_path: String,
    a_mode: String,
    b_mode: String,
    diff: String,
    new_file: bool,
    renamed_file: bool,
    deleted_file: bool,
}

#[derive(Deserialize, Debug)]
struct MRReferences {
    short: String,
    relative: String,
    full: String,
}

#[derive(Deserialize, Debug)]
struct MRPatch {
    source_branch: String,
    target_branch: String,
    references: MRReferences,
    changes: Vec<Change>,
}

fn main() {
    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    handle
        .read_to_string(&mut buffer)
        .expect("Couldn't read email from stdin");

    let (headers, _) = parse_headers(buffer.as_bytes()).expect("Couldn't parse email headers");
    let project = headers
        .get_first_header("X-GitLab-Project-Id")
        .expect("Couldn't get gitlab project id header");
    let iid = headers
        .get_first_header("X-GitLab-MergeRequest-IID")
        .expect("Couldn't get gitlab project id header");

    let resp = ureq::get(&format!(
        "https://gitlab.com/api/v4/projects/{}/merge_requests/{}/changes",
        project.get_value(),
        iid.get_value()
    ))
    .set(
        "Authorization",
        &format!(
            "Bearer {}",
            std::env::var("GL_TOKEN").expect("No GL_TOKEN found in env")
        ),
    )
    .call();

    let response = resp
        .into_json_deserialize::<MRPatch>()
        .expect("Couldn't parse merge request changes");

    let (fd, path) = unistd::mkstemp("/tmp/tempfile_XXXXXX").expect("Couldn't make temp file");
    let mut lines: Vec<String> = Vec::new();
    for change in response.changes {
        lines.push(format!(
            "diff --git a/{} b/{}\n",
            change.old_path, change.new_path
        ));
        if change.renamed_file {
            lines.push(format!("rename from {}\n", change.old_path));
            lines.push(format!("rename to {}\n", change.new_path));
        }

        if change.deleted_file {
            lines.push(format!("deleted file mode {}\n", change.a_mode));
        }

        if change.new_file {
            lines.push(format!("new file mode {}\n", change.b_mode));
            lines.push("--- /dev/null\n".into());
        } else {
            lines.push(format!("--- a/{}\n", change.old_path));
        }

        if change.deleted_file {
            lines.push("+++ /dev/null\n".into());
        } else {
            lines.push(format!("+++ b/{}\n", change.new_path));
        }

        if !change.new_file && !change.deleted_file && change.a_mode != change.b_mode {
            lines.push(format!("old mode {}\n", change.a_mode));
            lines.push(format!("new mode {}\n", change.b_mode));
        }

        lines.push(format!("{}\n", change.diff));
    }

    for line in lines {
        unistd::write(fd, line.as_bytes()).expect("Error writing file");
    }
    unistd::unlink(path.as_path()).expect("Couldn't unlink temp file");

    let _ = unistd::execvp(
        &CString::new(b"nvim".to_vec()).unwrap(),
        &[
            &CString::new(b"nvim".to_vec()).unwrap(),
            &CString::new(format!("/dev/fd/{}", fd)).unwrap(),
            &CString::new(format!("+f Patch: {}", response.references.full,)).unwrap(),
        ],
    );
}
